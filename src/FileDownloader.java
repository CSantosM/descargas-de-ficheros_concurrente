import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;

public class FileDownloader {

	// Lectura
	Reader r;
	BufferedReader br;

	private final String url = "https://dl.dropboxusercontent.com/u/1784661/download_list.txt";
	private final String nameDownload = "url_list.txt";
	private File dir = new File("downloads");
	private File fileDownload = new File("downloads/"+nameDownload);
	
	private int count = 0;
	private int nThreads;
	private String urlFile = null;
	private String nameFile = null;
	private int parts;
	private int threadConcurrente = 0;
	
	List<Thread> threads = new ArrayList<>();
	private Thread reader;
	private Thread merge;
	
	private Lock cerrojoContador = new ReentrantLock();
	private Lock cerrojoDescarga = new ReentrantLock();
	private CyclicBarrier barreraDescarga;
	private CyclicBarrier barreraMerge;
	private CyclicBarrier lector;
	

	public FileDownloader(int maxDown) throws IOException {
		
		dir.mkdir();
		fileDownload();
		
		initReader();
		this.barreraDescarga = new CyclicBarrier(maxDown+1); //Inicializo la barrera al numero de hilos de descarga + lector
		this.barreraMerge= new CyclicBarrier(maxDown+2); //Cuando todos los hilos de descarga + lector se completen se iniciar� el merge
		this.nThreads = maxDown;
		this.lector = new CyclicBarrier(maxDown+1);

	}

	public void process() {

		reader = new Thread(() -> read(),"Reader"); // hilo que lee
		
		for (int i = 0; i < this.nThreads; i++) { //hilos descarga
			threads.add(new Thread(() -> startDownload(), "hilo" + i));

		}
		this.merge = new Thread(() -> mergear(), "Merger"); //hilo que mergea
		
		reader.start();
		for (Thread th : threads) {
			th.start();
		}
		this.merge.start();
		
		try {
			reader.join();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		for (Thread th : threads) {
			try {
				th.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {
			this.merge.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
	
	
	private void read(){
		String str;
		try {
			while ((str = br.readLine())!=null){
				
				String [] words = str.split("\\s+");
				this.urlFile = words[0];
				this.nameFile = words[1];
				this.parts = Integer.parseInt(words[2]);
				System.out.println("Descargando " + this.nameFile + " por favor espere...");
				this.barreraDescarga.await(); // inicia procesos de descarga
				this.barreraMerge.await(); // cuando las descargas terminen inicia merge
			}
			
		} catch (IOException | InterruptedException | BrokenBarrierException e) {
			e.printStackTrace();
		}
		
	}
	
	private void startDownload(){
		
			try {
				while(this.count <= this.parts){
					
					if(this.count == 0){
						this.barreraDescarga.await();
					}
					
					this.cerrojoContador.lock();
					this.threadConcurrente++;
					
					if(this.threadConcurrente > this.parts){
						this.cerrojoContador.unlock();
						this.barreraMerge.await();
						this.lector.await();
						if(!this.reader.isAlive()){
							break;
						}
					}else{
						this.cerrojoContador.unlock();
						download(this.urlFile,this.nameFile,this.parts);//inicio descargas con url,name y parts
						
						if(this.count == this.parts){
							this.barreraMerge.await();//inicia el merge
							this.lector.await(); //espera a que el merge acabe
							
							if(!this.reader.isAlive()){
								break;							
							}
						}
					}	
					
				}
			} catch (InterruptedException | BrokenBarrierException | IOException e) {
				e.printStackTrace();
			}
		}
	
	
	private void download(String url, String name, int parts) throws IOException{
		this.cerrojoDescarga.lock();
		System.out.println("Descargando parte: "+this.count);
		int npart = this.count;
		this.count++;
		this.cerrojoDescarga.unlock();//libero el cerrojo porque ya he aumentado el contador
		
			URL website = new URL(url+"/"+name+".part"+npart);
			InputStream in = website.openStream();
			
			Path pathOut = Paths.get("downloads/" + name+".part"+npart);
			
			Files.copy(in, pathOut, StandardCopyOption.REPLACE_EXISTING);
			in.close();
		
	}

	public void mergeFile(String dir, String fileStart) throws InterruptedException, BrokenBarrierException {
		//this.barreraInicializar.await();
		System.out.println();
		System.out.println("\t Archivo "+ fileStart + " descargado correctamente en la carpeta downloads");
		System.out.println();
		File ofile = new File(dir + "/" + fileStart);
		FileOutputStream fos;
		FileInputStream fis;
		byte[] fileBytes;
		int bytesRead = 0;
		String[] files = new File(dir).list((path, name) -> Pattern.matches(
				fileStart + Pattern.quote(".") + "part.*", name));
		try {
			fos = new FileOutputStream(ofile, true);
			for (String fileName : files) {
				File f = new File(dir + "/" + fileName);
				//System.out.println(f.getAbsolutePath());
				fis = new FileInputStream(f);
				fileBytes = new byte[(int) f.length()];
				bytesRead = fis.read(fileBytes, 0, (int) f.length());
				assert (bytesRead == fileBytes.length);
				assert (bytesRead == (int) f.length());
				fos.write(fileBytes);
				fos.flush();
				fileBytes = null;
				fis.close();
				f.delete();
				fis = null;
			}
			
			fos.close();
			fos = null;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	private void mergear(){
		while(this.reader.isAlive()){
			try {
				this.barreraMerge.await();//espera hasta que las descargas se completen
				mergeFile("downloads",this.nameFile);
				this.count=0;
				this.threadConcurrente=0;
				this.lector.await(); // libera hilos de descarga
			} catch (InterruptedException | BrokenBarrierException e) {
				e.printStackTrace();
			}
		}
		System.out.println("----------------------------------- Gestor de descargas --------------------------------------");
		System.out.println("\t Descargas finalizadas, hasta la proxima");
	
	}
	
	//Descargo el fichero de urls
	public void fileDownload() {
		try{
			URLConnection conn = new URL(url).openConnection();
			conn.connect();
			System.out.println("\nempezando descarga: \n");
			System.out.println(">> URL: " + url);
			System.out.println(">> Nombre: " + nameDownload);
			InputStream in = conn.getInputStream();
			OutputStream out = new FileOutputStream(fileDownload);
			int b = 0;
			while (b != -1) {
			  b = in.read();
			  if (b != -1)
			    out.write(b);
			}
			out.close();
			in.close();
		} catch (IOException e) {
			  System.out.println("la url: " + url + " no es valida!");
		} 
	}

	// Inicializo archivos de lectura
	private void initReader() throws FileNotFoundException {
		r = new FileReader(fileDownload);
		br = new BufferedReader(r);
	}

}
